import java.util.Stack;

/**
 * Created by User on 12/12/2016.
 */
class Direction{
    int x;
    int y;
    public Direction(int x,int y){
        this.x = x;
        this.y = y;
    }
}

class Term{
    int x,y;
    Stack<Direction> dirs = new Stack<>();
    public Term(int x,int y,Stack<Direction> dirs){
        this.x = x;
        this.y = y;
        this.dirs = dirs;
    }
}

public class Tour1 {
    int fv = 0;
    int fh = 0;
    int priBoard[][];
    boolean end;
    int moveCount = 0;
    int n;
    int countBoard[][];
    boolean board[][] ;
    Direction dir[] = new Direction[8];
    Stack<Term> terms = new Stack<>();
    public Tour1(int n){
        dir[0] = new Direction(1,2);
        dir[1] = new Direction(1,-2);
        dir[2] = new Direction(-1,2);
        dir[3] = new Direction(-1,-2);
        dir[4] = new Direction(2,1);
        dir[5] = new Direction(-2,1);
        dir[6] = new Direction(2,-1);
        dir[7] = new Direction(-2,-1);
        priBoard = new int[n][n];
        board = new boolean[n][n];
        countBoard = new int[n][n];
        this.n = n;
    }

    public boolean possibleMove(int x,int y,Direction dir){
        if (x+dir.x <= n-1 && y+dir.y <= n-1 && x+dir.x >= 0 && y+dir.y >= 0)
            if  (!board[x+dir.x][y+dir.y])
                return true;
        return false;
    }

    public boolean checkLastMove(int x,int y){
        for (int i = 0; i <= 7; i++) {
            if (x+dir[i].x == fv && y+dir[i].y == fh) return true;
        }
        return false;
    }

    //hosein--------------
    ////nesbat dadan adada be array
    //start
    public int[][] Priority(int n) {
        int a[][] = new int[n][n];
        int v[] = {1, 1, 2, 2, -2, -2, -1, -1};
        int h[] = {2, -2, 1, -1, -1, 1, 2, -2};

        for (int i = 0; i < n; i++) {
            for (int j = 0; j < n; j++) {
                int count = 0;
                for (int k = 0; k < 8; k++) {
                    if (canMove(i + h[k], j + v[k], n))
                        count++;
                }
                a[i][j] = count;
            }
        }
        return a;
    }

    public boolean canMove(int row, int col, int N) {
        if (row >= 0 && col >= 0 && row < N && col < N && !board[row][col]) {
            return true;
        }
        return false;
    }
    //end

    public Stack<Direction> possibleDirs(int x, int y) {
        Stack<Direction> stack = new Stack<>();
        int P[][] = Priority(n);
        int min = 10;

        for (int j = 0; j < 8; j++) {
            if (canMove(x + dir[j].x, y + dir[j].y, n))
                if (P[x + dir[j].x][y + dir[j].y] <= min) {
                    if (P[x + dir[j].x][y + dir[j].y] == min) {
                        stack.push(dir[j]);
                    } else {
                        while (!stack.empty())
                            stack.pop();
                        min = P[x + dir[j].x][y + dir[j].y];
                        stack.push(dir[j]);
                    }
                }
        }
        //System.out.println(min);
        return stack;
    }
    //hossein-------------------------------


    public void move(int x,int y,Stack<Direction> dirs) {
        try {
            if (moveCount < n * n - 1) {
                if (!board[x][y] && dirs.isEmpty()) {
                    Stack<Direction> temp;
                    temp = possibleDirs(x, y);
                    Direction dir = temp.pop();
                    board[x][y] = true;
                    terms.push(new Term(x, y, temp));
                    countBoard[x][y] = moveCount;
                    moveCount++;
                    System.out.println(temp.toString());
                    System.out.println("success  " + x + "  " + y);
                    move(x + dir.x, y + dir.y, new Stack<>());
                } else if (board[x][y] && !dirs.isEmpty()) {
                    Direction t = dirs.pop();
                    terms.push(new Term(x, y, dirs));
                    countBoard[x][y] = moveCount;
                    moveCount++;
                    System.out.println("another Way");
                    move(t.x + x, t.y + y, new Stack<>());
                } else if (board[x][y] && dirs.isEmpty()) {
                    board[x][y] = false;
                    Term t = terms.pop();
                    moveCount--;
                    System.out.println("Backtrack");
                    move(t.x, t.y, t.dirs);
                }
            } else {
                if (!checkLastMove(x, y)) {
                    board[x][y] = false;
                    Term t = terms.pop();
                    moveCount--;
                    System.out.println("Checkin last move");
                    move(t.x, t.y, t.dirs);
                } else {
                    countBoard[x][y] = moveCount;
                    board[x][y] = true;
                    System.out.println(x + " " + y);
                    end = true;
                }
            }
        }catch (StackOverflowError sofe){
            System.out.println("Not Possible!");
        }

    }

    public void start(int x,int y){
        board[x][y]= true;
        fv=x;fh=y;
        move(x,x,possibleDirs(x,y));
    }
}
